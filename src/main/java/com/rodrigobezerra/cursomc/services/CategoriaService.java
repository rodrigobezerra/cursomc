/**
 * 
 */
package com.rodrigobezerra.cursomc.services;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rodrigobezerra.cursomc.domain.Categoria;
import com.rodrigobezerra.cursomc.repositories.CategoriaRepository;
import com.rodrigobezerra.cursomc.services.exceptions.ObjectNotFoundException;

/**
 * @author rodrigo.bezerra
 * 
 */
@Service
public class CategoriaService {
	
	/* Injeção de dependência com Spring 
	 * através da anotação @Autowired */
	@Autowired
	private CategoriaRepository repo;

	public Categoria buscar(Integer id) {
		Optional<Categoria> obj = repo.findById(id);
		return obj.orElseThrow(() -> new ObjectNotFoundException(
				"Objeto não encontrado! Id: " + id + ", Tipo: " + Categoria.class.getName()));
	}
	
}
