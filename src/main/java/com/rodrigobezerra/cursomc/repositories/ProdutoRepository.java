/**
 * 
 */
package com.rodrigobezerra.cursomc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rodrigobezerra.cursomc.domain.Produto;

/**
 * @author rodrigo.bezerra
 *
 */
@Repository
public interface ProdutoRepository extends JpaRepository<Produto, Integer>{
	
	
	
}
