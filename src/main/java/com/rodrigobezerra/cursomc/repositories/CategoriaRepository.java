/**
 * 
 */
package com.rodrigobezerra.cursomc.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.rodrigobezerra.cursomc.domain.Categoria;

/**
 * @author rodrigo.bezerra
 *
 */
@Repository
public interface CategoriaRepository extends JpaRepository<Categoria, Integer>{
	
	
	
}
